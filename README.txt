Synopsis:
This module implements the Geckoboard Push module's hooks and provides
datasets containing statistics about for system load average (Geck-o-meter
widget), watchdog error message levels (RAG widget), and will be extended
to include more.

Instructions:
Refer to the geckoboard_push (http://drupal.org/project/geckoboard_push)
project page, or README.txt for how to use.
